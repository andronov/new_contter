# NPM scripts:
+ `build:cordova:ios` - Build Cordova version (iOS)
+ `build:cordova:android` - Build Cordova version (Android)
+ `build` - Build production app
+ `build:debug` - Build production app without optimization and with debug options
+ `start` - Run development server
+ `start:prod` - Serve _dist_ directory

# Directories
+ _app_ - App source code
+ _dist_ - Build destination

