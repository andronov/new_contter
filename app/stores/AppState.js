import { observable, computed } from 'mobx';
import BaseClass from './BaseClass';
import { rgba } from '../services/utils';
import { GET_USER_URL, GET_SOURCES_WAlL,
         TO_ADD_LINK } from '../services/constants';
import { RestApi } from '../services/RestApi';
import User from './User';
import Site from './Site';

import Router from './Router';

class AppState extends BaseClass {
  router = Router.create();

  @observable currentUser;
  @observable users = [];

  @observable app_side = 'main';
  @observable app_content = 'main';

  @observable shareItem = null;
  @observable shareOpen = false;

  @observable pageSize;
  @observable anonymus = true;
  @observable searchWallSources = [];
  @observable loadingAddWallSearch = false;
  @observable errorAddWallSearch = false;
  @observable noElseAddWallSearch = false;
  @observable color = null;
  @observable colorOpacity = null;
  @observable backOpacity = null;

  @observable savingAddWall = false;
  @observable errorAddWall = false;

  @observable savingEditWall = false;
  @observable errorEditWall = false;

  @observable loadingProfile = false;
  @observable loadingGlobalItems = false;

  @observable bookmarkTags = [
    {name: 'sports', id: 1, fontSize: 15},
    {name: 'купибатон', id: 2, fontSize: 12},
    {name: 'лигачемпионов', id: 3, fontSize: 16},
    {name: 'ясмотрюфутбол', id: 4, fontSize: 14},
    {name: 'fcbjuve', id: 5, fontSize: 14},
    {name: 'чтобыбылолучше', id: 6, fontSize: 11},
    {name: 'conttercom', id: 7, fontSize: 16},
    {name: 'thebestofthebest', id: 8, fontSize: 12},
    {name: '🌝', id: 9, fontSize: 12}
  ];

  @observable colors = [
    {'id': 0, 'color': 'e53935'},
    {'id': 0, 'color': 'eb3f79'},
    {'id': 0, 'color': 'aa00ff'},
    {'id': 0, 'color': '7d58c2'},
    {'id': 0, 'color': '5b6abf'},
    {'id': 0, 'color': '1c88e3'},
    {'id': 0, 'color': '029ae5'},
    {'id': 0, 'color': '00abbf'},
    {'id': 0, 'color': '00887a'},
    {'id': 0, 'color': '679e38'},
    {'id': 0, 'color': 'f8a724'},
    {'id': 0, 'color': 'ff6f42'},
    {'id': 0, 'color': '8b6d62'},
    {'id': 0, 'color': '778f9b'},
    {'id': 0, 'color': '414141'},

    {'id': 0, 'color': 'e53932'},
  ];


  isMobile() {
    //return true;
   return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  }

  async toAddLink(value) {
    RestApi({value: value}, TO_ADD_LINK).then(response => {
      console.log('toShareLink', response.data,);
    }).catch(error => {
      console.log('error', error)
    });
  }

  async getUser(username) {
    RestApi({username: username}, GET_USER_URL).then(response => {

      let user = User.create(response.data.data);
      this.users.push(user);
      this.update({loadingProfile: false, color: user.color});
      user.loadItems('#', 0, 15);
      console.log('get2User', response.data, user);
      if(user !== this.currentUser) {
        user.loadAllUserWalls();
        if(user.is_site){
          user.loadSuggestUsers();
        }
      }

      /*response.data.walls.slice().forEach(wl => {
        Wall.create({...wl, user: this});
      });*/
    }).catch(error => {
      this.loadingProfile = false;
      console.log('error', error)
    });
  }

  async getAddWallSearch(query) {
    this.update({loadingAddWallSearch: true,
      searchWallSources: []});
    RestApi({query: query}, GET_SOURCES_WAlL).then(response => {
      console.log('getAddWallSearch', response.data);
      response.data.data.slice().forEach(item => {
        if(item.type === 'SITE'){
          this.searchWallSources.filter(it=>it.type === 'SITE').
          forEach(it=>{
            if(it.id !== item.id){
              this.searchWallSources.push(Site.create({...item}))
            }
          });
        }
        else{
          this.searchWallSources.filter(it=>it.type !== 'SITE').
          forEach(it=>{
            if(it.id !== item.id){
              this.searchWallSources.push(User.create({...item}))
            }
          });
        }
      });
    }).catch(error => {
      this.update({loadingAddWallSearch: false,
        errorAddWallSearch: true});
      console.log('error', error)
    });
  }

  get_color = (opacity, item, cl) => {
    // TODO сделать проверку на стены и на чужих профилях
    let color = 'fff';
    if(item){
      color = item.color
    }
    else if(this.currentUser && this.currentUser.activeWall){
      color = this.currentUser.activeWall.color;
    }
    else if(this.users.filter(us => us.username === this.get_userpath()).length){
      color = this.users.filter(us => us.username === this.get_userpath())[0].color
    }
    else if(this.currentUser){
      color = this.currentUser.color;
    }
    if(cl){color = cl}
    /*let c = rgba(color, opacity ? opacity: 1);
    switch(opacity) {
      case 1:
        this.color = c;
        break;
      case '0.20':
        this.colorOpacity = c;
        break;
      case '0.07':
        this.backOpacity = c;
        break;
    }*/
    if(!color){color = 'ffffff'}
    return rgba(color, opacity ? opacity: 1)
  };

  get_userpath = () => {
    if(window.matchRoute && window.matchRoute.path.startsWith('/:username')){
      return window.matchRoute.params.username
    }
    return ''
  }

}

export default AppState.create();
