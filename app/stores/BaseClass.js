import { action, toJS } from 'mobx';

const cache = {};

export default class BaseClass {

  // создание сообщения (или вытаскиваем из кеша если есть такой айдишник), добавляем в кеш и  присваиваем поля переданным объектом
  static create(obj, notCheckForCache) {
    let newObj;
    const updates = {};
    if (!notCheckForCache && obj && obj.id && cache[this.table] && cache[this.table][obj.id]) {
      newObj = cache[this.table][obj.id];
    } else {
      newObj = new this();
      if (!notCheckForCache && newObj.id) {
        if (!cache[this.table]) cache[this.table] = {};
        cache[this.table][newObj.id] = newObj;
      }
      for (const field in newObj) {
        if (typeof newObj[field] !== 'function') {
          updates[field] = newObj[field];
        }
      }
    }
    // если свойства являются связями то тоже проверяем их наличие и вытаскиваем из кеша
    if (obj) {
      const schema = this.schema && this.schema();
      Object.keys(obj).forEach(field => {
        const schemaField = schema && schema[field];
        if (schemaField && schemaField.type == 'one' && (typeof obj[field] === 'string' || typeof obj[field] === 'number') && cache[schemaField.factory.table] && cache[schemaField.factory.table][obj[field]]) {
          obj[field] = cache[schemaField.factory.table][obj[field]];
        }
        updates[field] = obj[field];
      });
    }

    return newObj._innerUpdate(updates, 'add');
  }


  update(obj) {
    this._innerUpdate(obj);
  }

  // удаление сообщения путем очистки его от всех ссылок на него (по списку описанной в схеме)
  delete() {
    const deleteUpdates = {};
    const schema = this.constructor.schema();
    Object.keys(schema).filter(field => { return schema[field].type == 'one' || schema[field].type == 'many'; }).forEach(field => {
      if (schema[field].type == 'many') {
        deleteUpdates[field] = [];
      } else {
        deleteUpdates[field] = null;
      }
    });
    return this._innerUpdate(deleteUpdates, 'delete');
  }

  // обновление объекта по списку полей в переданном объекте с добавлением ссылок на него по описанным в схеме связям - если one - то присваиваем или удаляем, если many - то добавляем или удаляем из массива
  @action
  _innerUpdate(obj, type) {
    // console.group(type || 'update', this.constructor.table, (new Date).toISOString().slice(11, -1));
    // console.log('this', toJS(this));
    // console.log('new', toJS(obj));

    if (obj.id && this.id !== obj.id) {
      if (!cache[this.constructor.table]) cache[this.constructor.table] = {};
      cache[this.constructor.table][obj.id] = this;
    }

    const schema = this.constructor.schema && this.constructor.schema();

    Object.keys(obj).forEach(field => {
      const fieldSchema = schema && schema[field];

      if (fieldSchema && fieldSchema.type == 'one' && (typeof obj[field] === 'string' || typeof obj[field] === 'number') && cache[fieldSchema.factory.table] && cache[fieldSchema.factory.table][obj[field]]) {
        obj[field] = cache[fieldSchema.factory.table][obj[field]];
      }

      if (type != 'add' && (this[field] === obj[field] || fieldSchema && fieldSchema.type == 'one' && typeof obj[field] === 'string' && this[field] && this[field].id == obj[field])) return;

      if (fieldSchema && fieldSchema.type == 'one') {
        var inverseFieldSchema = fieldSchema.inverse && fieldSchema.factory.schema()[fieldSchema.inverse];

        if (inverseFieldSchema && this[field] && typeof this[field] === 'object') {
          if (inverseFieldSchema.type == 'one' && this[field][fieldSchema.inverse]) {
            this[field][fieldSchema.inverse] = null;
          }

          if (inverseFieldSchema.type == 'many') {
            var index = this[field][fieldSchema.inverse].indexOf(this);
            if (index != -1) {
              this[field][fieldSchema.inverse].splice(index, 1);
              if (inverseFieldSchema.count) {
                if (!this[field][inverseFieldSchema.count]) {
                  throw new Error('no count field');
                }
                this[field][inverseFieldSchema.count]--;
              }
            }
          }
        }

        if (obj[field] && typeof obj[field] === 'object') {
          if (!(obj[field] instanceof BaseClass)) {
            obj[field] = schema[field].factory.create(obj[field]);
          }

          if (inverseFieldSchema && inverseFieldSchema.type == 'one' && obj[field][fieldSchema.inverse] !== this) {
            obj[field][fieldSchema.inverse] = this;
          }

          if (inverseFieldSchema && inverseFieldSchema.type == 'many') {
            var index = obj[field][fieldSchema.inverse].indexOf(this);
            if (index == -1) {
              obj[field][fieldSchema.inverse].push(this);
              if (inverseFieldSchema.count) {
                if (!obj[field][inverseFieldSchema.count]) obj[field][inverseFieldSchema.count] = 0;
                obj[field][inverseFieldSchema.count]++;
              }
            }
          }
        }
      }

      if (fieldSchema && fieldSchema.type == 'many') {
        obj[field].forEach((rec, i) => {
          if (!(rec instanceof BaseClass)) {
            obj[field][i] = schema[field].factory.create(rec);
          }
        });

        var inverseFieldSchema = fieldSchema.inverse && fieldSchema.factory.schema()[fieldSchema.inverse];

        if (inverseFieldSchema && this[field]) {
          this[field].forEach(rec => {
            if (obj[field].indexOf(rec) == -1 && rec[fieldSchema.inverse]) {
              rec[fieldSchema.inverse] = null;
            }
          });
        }
        if (inverseFieldSchema && obj[field]) {
          obj[field].forEach(rec => {
            if (rec[fieldSchema.inverse] !== this) {
              rec[fieldSchema.inverse] = this;
            }
          });
        }
      }
      this[field] = obj[field];
    });

    // console.log('result', toJS(this));
    // console.groupEnd();

    return this;
  }
}
