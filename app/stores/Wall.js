import { observable, computed } from 'mobx';
import BaseClass from './BaseClass';
import User from './User';
import Item from './Item';
import Site from './Site';
import AppState from './AppState';
import { GET_WALL_SOURCES, FOLLOW,
EDIT_WALL_SOURCES,
UPDATE_WALL, DELETE_WALL } from '../services/constants';
import { RestApi } from '../services/RestApi';

export default class Wall extends BaseClass {
  static table = 'walls';

  static schema() {
    return {
      user: {
        type: 'one',
        inverse: 'walls',
        factory: User,
      },
      items: {
        type: 'many',
        inverse: 'wall',
        factory: Item,
      }
    };
  }

  @observable id;
  @observable name;
  @observable icon;
  @observable current;
  @observable color;
  @observable slug;
  @observable userType;
  @observable sort;
  @observable home;
  @observable more;
  @observable words;
  @observable sources;
  @observable originalSources;
  @observable realtime;
  @observable slugwall;
  @observable description;
  @observable followers;
  @observable type;
  @observable user;
  @observable typeView = 'feed';
  @observable items = [];
  @observable shouldUpdated = 0;

  @observable listSources = [];
  @observable loadingListSources = false;
  @observable errorListSources = false;
  @observable savingFollowWall = false;
  @observable errorFollowWall = false;
  @observable loadingItems = false;
  @observable loadedItems = false;
  lastOffset = 0;
  lastLimit = 3;

  async getWallSources(query) {
    this.update({loadingListSources: true});

    RestApi({wall_id: this.id, source: this.sources}, GET_WALL_SOURCES).then(response => {
      console.log('getWallSources', response.data);
      this.update({loadingListSources: false});
      response.data.data.slice().forEach(item => {
        if(item.type === 'SITE'){
          let st = Site.create({...item});
          if(this.listSources.some(it => it.id === item.id && it.type === 'SITE'))return;
          this.listSources.push(st)
        }
        else{
          let us = User.create({...item});
          if(this.listSources.some(it => it.id === item.id && it.type === 'PROFILE'))return;
          this.listSources.push(us)
        }

      });
    }).catch(error => {
      this.update({loadingListSources: false,
        errorListSources: true});
      console.log('error', error)
    });
  }

  delete() {
    RestApi({id: this.id}, DELETE_WALL).then(response => {
      super.delete();
    }).catch(error => {
      console.log('error', error)
    });
  }

  async updateWall(data) {
    RestApi(data, UPDATE_WALL).then(response => {
      console.log('updateWall', response.data);
      this.update({...response.data.data, originalSources: response.data.data.sources});
      AppState.savingEditWall = false;
    }).catch(error => {
      console.log('error', error);
      AppState.savingEditWall = false;
      AppState.errorEditWall = true;
    });
  }

  async followUser(username) {
    this.savingFollowWall = true;
    RestApi({
      wall_id: this.id,
      username: username
    }, FOLLOW).then(response => {
      this.update({originalSources: response.data.wall.sources});
      //console.log('followUser', this.originalSources, response.data.wall.sources);
      //this.update({...response.data.data, originalSources: response.data.data.sources});
      //AppState.savingEditWall = false;
      this.savingFollowWall = false;
    }).catch(error => {
      console.log('error', error);
      this.savingFollowWall = false;
      this.errorFollowWall = true;
    });
  }

  async editWallSource(source, action) {
    RestApi({wall_id: this.id, source: source, action: action}, EDIT_WALL_SOURCES).then(response => {
      let elem = AppState.searchWallSources.filter(it => it.id == source.id && it.type === source.type);
      this.originalSources = response.data.data.sources;
      if(action && elem.length){
        this.listSources.push(elem[0])
      }
      else if(!action){
        let items = [];
        this.listSources.forEach((it, i) => {
          if(it.id == source.id && it.type === source.type){
          }
          else{
            items.push(it)
          }
        });
        if(items.length){this.listSources = items}
      }
    }).catch(error => {
      console.log('error', error)
    });
  }
}