import React from 'react';
import { getContext } from 'recompose';

const types = { storage: React.PropTypes.object };

export const withStorage = getContext(types);

export default class StorageProvider extends React.Component {
  static childContextTypes = types;
  static propTypes = types;

  componentWillMount() {
    const { storage } = this.props;
    this.storage = storage;
  }

  getChildContext() {
    const { storage } = this;
    return {
      storage,
    };
  }

  render() {
    return React.Children.only(this.props.children);
  }
}
