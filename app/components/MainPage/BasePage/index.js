import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';

import {Motion, spring} from 'react-motion';

const springSettings = {stiffness: 170, damping: 26};
const NEXT = 'show-next';

@inject("appState")
@observer
export default class BasePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      photos: [[400, 400], [800, 600], [800, 400], [800, 400]],
      amazonPhotos: ['https://images-na.ssl-images-amazon.com/images/I/61MQR8I9yTL._SL1010_.jpg',
        'https://images-na.ssl-images-amazon.com/images/I/71A0Ty5zPbL._SL1010_.jpg',
        'https://images-na.ssl-images-amazon.com/images/I/61lIQ47SIjL._SL1000_.jpg',
        'https://images-na.ssl-images-amazon.com/images/I/612H0butwPL._SL1001_.jpg'],
      currPhoto: 0,
      amazonClose: false
    };
  };

  handleChange = ({target: {value}}) => {
    this.setState({currPhoto: value});
  };

  clickHandler = (btn) => {
    let photoIndex = btn === NEXT ? this.state.currPhoto+1 : this.state.currPhoto-1;

    photoIndex = photoIndex >= 0 ? photoIndex : this.state.photos.length - 1;
    photoIndex = photoIndex >= this.state.photos.length ? 0 : photoIndex;

    this.setState({
      currPhoto: photoIndex
    })
  };

  render() {
    let {appState} = this.props;

    const {photos, currPhoto, amazonClose, amazonPhotos} = this.state;
    const [currWidth, currHeight] = photos[currPhoto];

    const widths = photos.map(([origW, origH]) => currHeight / origH * origW);

    const leftStartCoords = widths
      .slice(0, currPhoto)
      .reduce((sum, width) => sum - width, 0);

    let configs = [];
    photos.reduce((prevLeft, [origW, origH], i) => {
      configs.push({
        left: spring(prevLeft, springSettings),
        height: spring(currHeight, springSettings),
        width: spring(widths[i], springSettings),
      });
      return prevLeft + widths[i];
    }, leftStartCoords);

    console.log('amazonPhotos', amazonPhotos);

    return (
      <div className='bs'>
        <div className='bs__inner'>
          {/*<div className='bs__sites'>
            <div className="bs__site">
                base
            </div>
          </div>*/}

          <div className='bs__feed'>
            <div className='bs__feed_inner'>
              <div className='bs__feed_list'>

                <div className='bs__feed_item' style={{height: '325px', width: '600px'}}>

                  <div className="fd__layer" style={{
                    background: "url('/assets/media/promo/sw.gif') center/cover no-repeat"
                  }} />

                  <div className="fd__overlay" style={{
                    zIndex: 5,
                    background: 'linear-gradient(to bottom, rgba(30, 87, 153, 0) 0%, rgba(1, 3, 5, 0) 57%, rgba(0, 0, 0, 0.13) 59%, rgba(0, 0, 0, 0.68) 100%)'
                  }} />

                  <div className="fd__layer" style={{
                    zIndex: 6,
                    fontFamily: '"Open Sans", sans-serif',
                    fontSize: '35px',
                    fontWeight: 'bold',
                    color: 'white',
                    display: 'flex',
                    alignContent: 'flex-start',
                    justifyContent: 'flex-end',
                    flexDirection: 'column',
                    padding: '25px',
                  }} >
                    Швейцария
                  </div>

                  <div className="fd__layer" style={{
                    zIndex: 6,
                    fontFamily: '"Open Sans", sans-serif',
                    fontSize: '16px',
                    fontWeight: 'normal',
                    color: 'white',
                    display: 'flex',
                    alignContent: 'flex-start',
                    justifyContent: 'flex-end',
                    flexDirection: 'column',
                    padding: '25px',
                    paddingBottom: '65px'
                  }} >
                    5 туров от 49900 руб.
                  </div>

                </div>

                <div className='bs__feed_item'
                     onMouseLeave={() => {this.refs.video2.pause()}}
                     onMouseEnter={() => {this.refs.video2.play()}}
                     style={{height: '600px', width: '600px'}}>
                  <video ref="video2" style={{width: '600px'}}
                         autoPlay={true}
                         muted={true}>
                    <source src="/assets/media/promo/mufc.mp4"
                            type="video/mp4" muted={true}/>
                  </video>

                </div>

                <div className='bs__feed_item'
                     onMouseLeave={() => {this.refs.video.pause()}}
                     onMouseEnter={() => {this.refs.video.play()}}
                     style={{height: '338px', width: '600px'}}>
                  <video ref="video" style={{width: '600px'}}
                         muted={true}>
                    <source src="/assets/media/promo/video.mp4#t=30,40"
                            type="video/mp4" muted={true}/>
                  </video>

                  <div className='fd__layer' >
                    <div className="container_youtube">

                      <span className="before"></span>
                      <p className="you">You<span className="tube">Tube</span></p>
                      <span className="after"></span>

                    </div>
                  </div>

                </div>


                <div className='bs__feed_item' style={{height: '400px', width: '400px' }}>

                  <div className='fd__logo'
                       style={{
                         backgroundImage: "url('https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Amazon.com-Logo.svg/799px-Amazon.com-Logo.svg.png')",
                         height: '22px',
                         width: '115px'
                       }}/>

                  {/*
                   <div>Scroll Me</div>
                   <button onClick={this.clickHandler.bind(null, '')}>Previous</button>
                   <input
                   type="range"
                   min={0}
                   max={photos.length - 1}
                   value={currPhoto}
                   onChange={this.handleChange} />
                   <button onClick={this.clickHandler.bind(null, NEXT)}>Next</button>
                  */}

                  {/*<Motion style={{x: spring(amazonClose  ? 500 :150), y: spring(amazonClose  ? 0 : 50), z: spring(amazonClose  ? -10 : 50)}}>
                    {({x, y, z}) =>
                      <div style={{height: `${x}px`, width: `${x}px`, top: `${y}%`, right: `${z}px`, zIndex: 45}}
                           className='fd__slider'
                           onClick={()=>{this.setState({amazonClose: !amazonClose})}}>
                          <div>
                            <img width={'400px'} src="https://images-na.ssl-images-amazon.com/images/I/61MQR8I9yTL._SL1010_.jpg" />
                          </div>

                      </div>
                    }
                  </Motion>*/}

                  {amazonPhotos.map((item, i)=>{
                      return(
                        <Motion style={{x: spring(i === currPhoto ? 400 : 50), y: spring(i === currPhoto ? 0 : 50), z: spring(i === currPhoto ? 0 : 50)}}>
                          {({x, y, z}) =>
                            <img
                              onClick={this.clickHandler.bind(null, NEXT)}
                              style={{position: 'absolute', bottom: y, right: y, zIndex: (currPhoto===i ? 10 : i)}}
                              width={`${x}px`} src={item}/>
                          }
                        </Motion>
                      )
                  })}


                  {/*<div className='fd__slider'>
                    <img style={{height: '150px'}} src="https://images-na.ssl-images-amazon.com/images/I/614QpDSUm8L._SL1000_.jpg"/>
                  </div>

                  <Motion style={{height: spring(currHeight), width: spring(currWidth)}}>
                    {container =>
                      <div className="demo4-inner" style={container}>
                        {configs.map((style, i) =>
                          <Motion key={i} style={style}>
                            {style =>
                              <div className="demo4-photo"
                                   style={style} >
                                <div style={{
                                  background: "url('https://images-na.ssl-images-amazon.com/images/I/61MQR8I9yTL._SL1010_.jpg') center/cover no-repeat",
                                  ...style
                                }} />
                              </div>
                            }
                          </Motion>
                        )}
                      </div>
                    }
                  </Motion>

                  <img width={'400px'} src="https://images-na.ssl-images-amazon.com/images/I/61MQR8I9yTL._SL1010_.jpg" />*/}

                  <div className="fd__data">
                    <div className="fd__title" style={{fontFamily: "'Open Sans', sans-serif"}}>
                      COWIN E7 Active Noise Cancelling Bluetooth<br />Headphones with Microphone Hi-Fi Deep Bass</div>
                    <div className="fd__price" style={{fontFamily: "'Open Sans', sans-serif"}}>$39.15</div>
                  </div>
                  <div className='fd__buy'>
                    <div className='fd__buy_icon' />
                    <span>go to site</span>
                  </div>
                </div>

                <div className='bs__feed_item' style={{height: '540px', width: '290px', fontWeight: 300, marginRight: '10px',
                  fontFamily: "'Open Sans', sans-serif"}}>

                  <div className='fd__logo'
                       style={{
                         backgroundImage: "url('https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/ESPN_wordmark.svg/694px-ESPN_wordmark.svg.png')",
                         height: '15px',
                         width: '61px'
                       }}/>

                  <div className="fd__overlay" />

                  <div className="example-2 card">
                    <div className="wrapper" style={{
                      background: "url('http://a1.espncdn.com/combiner/i?img=%2Fphoto%2F2017%2F0425%2Fr203052_1296x864_3-2.jpg') center/cover no-repeat"
                    }}>
                      <div className="data">
                        <div className="content">
                          <h1 className="title"><a href="#">How Russell Westbrook became the $205 million face of Oklahoma City</a></h1>
                          <p style={{lineHeight: '22px'}} className="text">
                            Only two teammates, though, made an appearance: Josh Huestis and ... Russell Westbrook. Westbrook was even the first to arrive.
                          </p>
                          <a href="#" className="button">Read more</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className='bs__feed_item' style={{height: '540px', width: '300px', fontWeight: 300,
                  fontFamily: "'Open Sans', sans-serif"}}>

                  <div className='fd__logo'/>

                  <div className="example-2 card">
                    <div className="wrapper">
                      <div className="data">
                        <div className="content">
                          <span className="author">Jane Doe</span>
                          <h1 className="title"><a href="#">Stranger Things: The sound of the Upside Down</a></h1>
                          <p style={{lineHeight: '22px'}} className="text">
                            The antsy bingers of Netflix will eagerly anticipate the digital release of the Survive soundtrack, out today.
                          </p>
                          <a href="#" className="button">Read more</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className='bs__feed_item'>
                  <img width={'600px'} src="/assets/media/promo/ezgif-3-ef453be886.gif" />
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    );
  }
}
