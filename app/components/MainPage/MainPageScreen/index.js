import * as React from 'react';
import { observer, inject } from 'mobx-react';
import './style.scss';

@inject('appState')
@observer
export default class MainPageScreen extends React.Component {

  state = {
    canvas: null,
    context: null
  };

  componentDidMount() {
    const canvas = document.querySelector('.canvas');
    const ctx = canvas.getContext('2d');
    this.setState({
      canvas,
      context: ctx,
    });

    let bg = new Image();
    bg.src = 'https://aos.iacpublishinglabs.com/question/aq/700px-394px/long-college-basketball-game-last_30c9e417110f8258.jpg?domain=cx.aos.ask.com';
    bg.onload = function()
    {
      ctx.drawImage(bg, 0, 0);
    };

    let canvas2 = document.getElementById("canvas2");
    let ctx2 = canvas2.getContext("2d");

    let video = document.createElement("video");
    video.src = "https://media1.giphy.com/media/9QpOVYBqMXxCw/giphy.mp4";
    video.addEventListener('loadeddata', function() {
      console.log("loadeddata");
      video.play();
      videoLoop()
    });
    function videoLoop() {

      // put text on canvas
      ctx2.font = "bold 68pt Calibri";
      ctx.fillStyle = 'red';
      ctx2.textAlign = 'center';
      let x = canvas2.width / 2;
      ctx2.fillText("VOLCANO", x, 170);

      // use compositing to draw the background image only where the text has been drawn
      //ctx2.globalCompositeOperation = "source-in";

      //context.drawImage(video, xStart, yStart, xEnd - xStart, yEnd - yStart);
      ctx2.drawImage(video,0,0,450,250,0,0,450,250);
      // ctx2.drawImage(video, 0, 0, video.width, video.height, 0, 0, canvas2.width, canvas2.height);
      requestAnimationFrame(videoLoop);
    }
  }

  onLoadCanvas = (e) => {
    console.log('onLoadCanvas', e);
  };

  render() {

    return (
      <div style={{marginTop: '10px', marginLeft: '10px',
        height: '100%', overflow: 'auto'}} className=''>

        {/*<div style={{width: '785px', height: '540px'}} className="link">
          <div style={{color: 'white'}} className="link__inner">
            <div className="container clearfix">

              <div className="shoe-details-left">

                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/nike.svg" alt="logo" className="logo" />

                <div className="product-title">
                  Air Structure <strong>19</strong>
                </div>

                <div className="product-description">
                  Dynamic Support and Nike Zoom Air come together for a more supportive feel with high-speed responsiveness.
                </div>

                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/shoe.png" alt="shoe" className="product-shot animated fadeInLeft" />

                <div className="product-price">12,995</div>

                <div className="product-price-details">
                  EMI starts from 534 + Rs 100 Delivery
                </div>


              </div>

              <div className="shoe-details-right">

                <span className="product-title">Air Structure <strong>19</strong></span>
                <span className="label">New</span>

                <div className="product-category">
                  Running Collections
                </div>

                <div className="product-stars">
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star gray"></i>
                  <i className="fa fa-star gray"></i>
                </div>

                <ul className="product-tabs">
                  <li className="active"><a href="#pane1">Description</a></li>
                  <li><a href="#pane2">Details</a></li>
                  <li><a href="#pane3">Reviews</a></li>
                  <li><a href="#pane4">Size & Fit</a></li>
                </ul>

                <div className="tab-content">
                  <div id="pane1" className="tab-pane active">
                    Dynamic Support and Nike Zoom Air come together for a more supportive feel with high-speed responsiveness.
                  </div>
                  <div id="pane2" className="tab-pane">

                  </div>

                  <div id="pane3" className="tab-pane">

                  </div>

                  <div id="pane4" className="tab-pane">

                  </div>
                </div>

                <h2>Size</h2>
                <ul className="sizes">
                  <li><a href="#">07</a></li>
                  <li><a href="#">08</a></li>
                  <li className="active"><a href="#">09</a></li>
                  <li><a href="#">10</a></li>
                  <li><a href="#">11</a></li>
                </ul>

                <h2>Colour</h2>
                <ul className="colours">
                  <li className="active"><a href="#"><span className="circle blue"></span></a></li>
                  <li><a href="#"><span className="circle orange"></span></a></li>
                  <li><a href="#"><span className="circle yellow"></span></a></li>
                  <li><a href="#"><span className="circle black"></span></a></li>
                </ul>

                <a href="#" className="btn btn-primary">
                  Add to Cart
                </a>

                <a href="#" className="btn btn-secondary">
                  Wishlist
                </a>

              </div>

              <div className="close-button">&times;</div>

            </div>
          </div>
        </div>*/}


        <div style={{width: '445px', height: '250px'}} className="link">
          <div style={{color: 'white'}} className="link__inner">

            <div style={{zIndex: '20'}} className="link__layer">
              <canvas
                      className='canvas2'
                      id='canvas2' width="450px" height="250px" />
                {/*<video style={{height: '250px'}} poster="//i.imgur.com/67kilzih.jpg" preload="auto" autoPlay muted="muted" loop="loop" >
                <source src="//i.imgur.com/67kilzi.mp4"
                        type="video/mp4" />
              </video>*/}
            </div>

            {/* <div style={{zIndex: '50', height: '100%', width: '30%', background: 'black'}} className="link__layer">
            </div>*/}

            {/*<div style={{zIndex: '60', top: '10%'}} className="link__layer">
              Leonardo DiCaprio gets his Oscar engraved
            </div>*/}


          </div>
        </div>

        <div style={{width: '470px', height: '480px'}} className="link">
          <div className="link__inner">

            <div className="carder carder--19" style={{left: '50%'}}>
              <div className="carder__header carder__header--19">
                <div className="carder__watermark" data-watermark="Air" />

                <img src="https://s32.postimg.org/7b31lbyit/nike.png" alt="Nike" className="carder__logo carder__will-animate" />

                  <span className="carder__price carder__will-animate">$120</span>

                  <h1 className="carder__title carder__will-animate">Air Structure 1</h1>
                  <span className="carder__subtitle carder__will-animate">
                    From the Flymesh upper to the triple-density foam midsole, the Nike Air Zoom Structure 19 Men's Running Shoe offers plenty of support and the response you need for a smooth, stable ride that feels ultra fast.
                  </span>

              </div>

              <div className="carder__body">
                <img src="https://s32.postimg.org/jqzrf2rut/nike19.png" alt="Nike 19" className="carder__image carder__will-animate" />
                  <div className="carder__wish-list carder__wish-list--19 carder__will-animate">Wish List</div>

                  <span className="carder__category carder__will-animate">Men's running shoe</span>
              </div>
            </div>

          </div>
        </div>

        <div style={{width: '445px', height: '250px'}} className="link">
          <div style={{color: 'white'}} className="link__inner">

            <div style={{zIndex: '20'}} className="link__layer">
              <video style={{height: '250px'}} poster="//i.imgur.com/VIEA2AEh.jpg" preload="auto" autoPlay muted="muted" loop="loop" >
                <source src="https://i.imgur.com/VIEA2AE.mp4" type="video/mp4" />
              </video>
            </div>

            <div style={{zIndex: '40', top: '10%'}} className="link__layer">
              Leonardo DiCaprio gets his Oscar engraved
            </div>


          </div>
        </div>

        <div style={{width: '425px', height: '540px'}} className="link">
          <div className="link__inner">
              <div className="example-2 card">
                <div className="wrapper">
                  <div className="header">
                    <div className="date">
                      <span className="day">12</span>
                      <span className="month">Aug</span>
                      <span className="year">2016</span>
                    </div>
                    <ul className="menu-content">
                      <li>
                        <a href="#" className="fa fa-bookmark-o"></a>
                      </li>
                      <li><a href="#" className="fa fa-heart-o"><span>18</span></a></li>
                      <li><a href="#" className="fa fa-comment-o"><span>3</span></a></li>
                    </ul>
                  </div>
                  <div className="data">
                    <div className="content">
                      <span className="author">Jane Doe</span>
                      <h1 className="title"><a href="#">Stranger Things: The sound of the Upside Down</a></h1>
                      <p className="text">The antsy bingers of Netflix will eagerly anticipate the digital release of the Survive soundtrack, out today.</p>
                      <a href="#" className="button">Read more</a>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>

        <div style={{width: '225px', height: '300px'}} className="link">
          <div style={{color: 'white'}} className="link__inner">

            <div style={{zIndex: '20'}} className="link__layer">
              <img style={{height: '300px'}} src="https://i.redditmedia.com/cg2fkc3NolGCtuAnTh7Rt_YqYAP9NVymZWesfTvIGt8.png?w=576&s=bdfe5ddf1ade445e2e9cbc7ffcf27b73" />
            </div>

            <div style={{zIndex: '40', bottom: '10%'}} className="link__layer">
              "I'm a beer now!"
            </div>


          </div>
        </div>

        <div className="link">
          <div style={{color: 'white'}} className="link__inner">


            <div style={{zIndex: '30'}} className="link__layer link__layer_bg">

            </div>

            <div style={{zIndex: '20'}} className="link__layer">
              <img style={{width: '450px'}} src="https://upload.wikimedia.org/wikipedia/commons/5/53/Napoleon_Paul_Delaroche.jpg" />
            </div>

            <div style={{zIndex: '40', top: '10%'}} className="link__layer">
              Наполеон I — Википедия
            </div>

            <div style={{zIndex: '40', bottom: '10%'}} className="link__layer">
              Наполео́н I Бонапа́рт — император французов в 1804—1814 и 1815 годах, великий полководец и государственный деятель, заложивший основы современного французского государства.
            </div>


          </div>
        </div>

        <div style={{width: '445px', height: '450px'}} className="link">
          <div className="link__inner">

            <div style={{zIndex: '20'}} className="link__layer">
              <video style={{height: '450px'}} poster="//i.imgur.com/zRWKsznh.jpg" preload="auto" autoPlay muted="muted" loop="loop" >
                <source src="https://i.imgur.com/zRWKszn.mp4" type="video/mp4" />
              </video>
            </div>

            <div style={{zIndex: '40', bottom: '10%'}} className="link__layer">
              Just a dapper prairie dog eating cheese
            </div>


          </div>
        </div>



        <div className="link">
          <div className="link__inner">


            <div style={{zIndex: '30'}} className="link__layer link__layer_bg">

            </div>

            <div style={{zIndex: '40', top: '10%', textAlign: 'center'}} className="link__layer">
              EASTERN CONFERENCE FINALS - GAME 1 - CLE leads 1-0
            </div>

            <div style={{zIndex: '40', left: '10%', top: '30%'}} className="link__layer">
              <img style={{}} src="http://a.espncdn.com/combiner/i?img=/i/teamlogos/nba/500/cle.png&h=100&w=100" />
            </div>

            <div style={{zIndex: '40', right: '10%', top: '30%'}} className="link__layer">
              <img style={{}} src="http://a.espncdn.com/combiner/i?img=/i/teamlogos/nba/500/bos.png&h=100&w=100" />
            </div>

            <div style={{zIndex: '40', left: '36%', top: '46%'}} className="link__layer">
              117
            </div>

            <div style={{zIndex: '40', right: '36%', top: '46%'}} className="link__layer">
              104
            </div>

            <div style={{zIndex: '50', bottom: '10%', fontSize: '12px'}} className="link__layer">
              <a>Cavaliers vs. Celtics - Game Summary - May 17, 2017 - ESPN</a>
            </div>

            <canvas style={{display: 'none'}}
                    className='canvas'
                    id='canvas' width="450px" height="350px">

            </canvas>
          </div>
        </div>
      </div>
    );
  }
}