import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';

let items = [
  {
    id: 1,
    width: 25
  }
];

@inject("appState")
@observer
export default class LandingScreen extends React.Component {

  state = {
    items: items
  };

  componentDidMount () {
    this.setCanvasAnimation()
  }

  setCanvasAnimation = () => {
    //canvas_sd3fI;

    let canvas = document.querySelector('#canvas_sd3fI');

    console.log('canvas', canvas, canvas.innerWidth, canvas.innerHeight);
    //C:\my\new_contter\assets\media\images\wimbledon-tennis-openroof.jpg

    if(canvas.getContext) {
      let ctx = canvas.getContext('2d');
      let w = canvas.width;
      let h = canvas.height;
      ctx.strokeStyle = 'rgba(174,194,224,0.5)';
      ctx.lineWidth = 1;
      ctx.lineCap = 'round';

      let img = new Image();
      img.onload = start;
      img.src = "/assets/media/promo/sdfgsdfg45g.jpg";

      function start() {
        w = img.width;
        h = img.height;
        setInterval(draw, 50);
      }

      let init = [];
      let maxParts = 500;
      for(let a = 0; a < maxParts; a++) {
        init.push({
          x: Math.random() * w,
          y: Math.random() * h,
          l: Math.random() * 1,
          xs: -4 + Math.random() * 4 + 2,
          ys: Math.random() * 10 + 10
        })
      }

      let particles = [];
      for(let b = 0; b < maxParts; b++) {
        particles[b] = init[b];
      }

      function draw() {
        //console.log('s', 0, 0, w, h);
        ctx.clearRect(0, 0, w, h);
        ctx.drawImage(img, 0, 0);

        ctx.font = "24px Calibri";
        ctx.fillStyle = "#ffffff";
        //ctx.fillText("Wimbledon: how does the weather", 10, h-70);
        //ctx.fillText("affect things?", 10, h-40);
        ctx.fillText("Wimbledon: How the weather really affects tennis", 10, h-40);

        for(let c = 0; c < particles.length; c++) {
          let p = particles[c];
          ctx.beginPath();
          ctx.moveTo(p.x, p.y);
          ctx.lineTo(p.x + p.l * p.xs, p.y + p.l * p.ys);
          ctx.stroke();
        }
        move();

      }

      function move() {
        for(let b = 0; b < particles.length; b++) {
          let p = particles[b];
          p.x += p.xs;
          p.y += p.ys;
          if(p.x > w || p.y > h) {
            p.x = Math.random() * w;
            p.y = -20;
          }
        }
      }



    }
  };

  render() {
    let {appState} = this.props;

    return (
      <div className='page__layout'>
        <div className="flex">
          <div className="flex__inner">

            <div className="flex__item blur" style={{width: '597px', height: '324px', top: '2px', left: '2px'}}>

              <div className="flex__item_bg" style={{width: '597px', height: '324px'}}>
              </div>
              <div className="flex__item_inner" style={{backgroundColor: 'red',
                backgroundImage: 'url(/assets/media/promo/dfgb43dfsg.jpg)',
                width: '597px', height: '324px'}}>
              </div>

            </div>

            <div className="flex__item element" style={{width: '300px', height: '450px', left: '601px', top: '2px'}}>

              <div className="flex__item_inner" style={{backgroundColor: 'green',
                backgroundImage: 'url(/assets/media/promo/6df$fdg.jpg)',
                width: '300px', height: '450px', left: '597px'}}>
              </div>

            </div>

            <div className="flex__item blur" style={{width: '317px', height: '450px', left: '903px', top: '2px'}}>

              <div className="flex__item_bg" style={{width: '317px', height: '450px'}}>
              </div>
              <div className="flex__item_inner" style={{backgroundColor: 'blue',
                backgroundImage: 'url(/assets/media/promo/nhg6thth.jpg)',
                width: '327px', height: '450px', left: '897px'}}>
              </div>

            </div>

            <div className="flex__item element" style={{width: '696px', height: '415px', left: '1222px', top: '2px'}}>

              <div className="flex__item_inner" style={{
                backgroundImage: 'url(/assets/media/promo/dfgsdfg4fsf.jpg)',
                width: '696px', height: '415px', left: '897px'}}>
                {/*<iframe width="696" height="415"
                        src="https://www.youtube.com/embed/zxO_6-zIzM0?list=RDzxO_6-zIzM0"
                        frameborder="0" allowfullscreen />*/}
              </div>

            </div>

            <div className="flex__item element"
                 style={{width: '597px', height: '324px', top: '328px', left: '2px'}}>
              <div className="flex__item_cn">
                <canvas
                  style={{width: '597px', height: '324px'}}
                  width="597px"
                  height="324px"
                  id="canvas_sd3fI" className="flex__item_canvas">

                </canvas>
                {/*<img src="/assets/media/promo/sdfgsdfg45g.jpg"/>*/}
              </div>

            </div>

            <div className="flex__item blur"
                 style={{background: 'blue', width: '619px', height: '405px', left: '601px', top: '454px'}}>

              <div className="flex__item_bg" style={{width: '619px', height: '450px'}}>
              </div>
              <div className="flex__item_inner" style={{backgroundColor: 'blue',
                backgroundImage: 'url(/assets/media/promo/cvxz234fvs.jpg)',
                width: '619px', height: '450px'}}>
              </div>

            </div>

            <div className="flex__item blur"
                 style={{background: 'blue', width: '696px', height: '100px', left: '1222px', top: '419px'}}>

              <div className="flex__item_bg" style={{width: '696px', height: '100px'}}>
              </div>
              <div className="flex__item_inner" style={{backgroundColor: 'blue',
                backgroundImage: 'url(/assets/media/promo/cvxz234fvs.jpg)',
                width: '696px', height: '100px'}}>
              </div>

            </div>

            <div className="flex__item "
                 style={{background: 'blue', width: '395pxx', height: '540px', left: '1222px', top: '521px'}}>

              <div className="flex__item_inner" style={{backgroundColor: 'blue',
                backgroundImage: 'url(/assets/media/promo/sdfsdiyo56h.jpg)',
                width: '395px', height: '540px'}}>
              </div>

            </div>

          </div>
        </div>
      </div>
    );
  }
}
