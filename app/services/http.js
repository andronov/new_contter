import axios from 'axios';
import { BASE_URL } from './constants';
import AuthStorage from './AuthStorage';

const get_token = function () {
  let jwt = AuthStorage.getSession();
  try {
    return { Authorization: `Bearer ${jwt}` };
  } catch (e) {
    return { Authorization: null };
  }
};
function http(config) {
  config = Object.assign({
    baseURL: BASE_URL,
    headers: {
      common: get_token(),
    },
  }, config);
  return axios(config);
}
export default http;
