import * as uuid from 'uuid';
const AUTH_DATA_KEY = 'auth_data';
class AuthStorage {
  constructor() {
    this.session = null;
    const sessionData = localStorage.getItem(AUTH_DATA_KEY);
    this.session = sessionData ?
            JSON.parse(sessionData) : { deviceId: uuid.v4() };
    this.save();
  }
  save() {
    localStorage.setItem(AUTH_DATA_KEY, JSON.stringify(this.session));
  }
  isLoggedIn() {
    return !!this.session.jwt;
  }
  setSession(session) {
    Object.assign(this.session, session);
    this.save();
  }
  getSession() {
    return localStorage.getItem('if-token');
    //return this.session;
  }
  clearSession() {
    this.setSession({jwt: null});
  }
}
export default new AuthStorage();
