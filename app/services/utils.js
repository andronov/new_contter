import uuid from 'uuid';

export function rgba(hex, opacity) {
  var rgb = hex.replace('#', '').match(/(.{2})/g);

  var i = 3;
  while (i--) {
    rgb[i] = parseInt(rgb[i], 16);
  }

  if (typeof opacity == 'undefined') {
    return 'rgb(' + rgb.join(', ') + ')';
  }

  return 'rgba(' + rgb.join(', ') + ', ' + opacity + ')';
};

export function stringifyValues(object) {
  return Object.keys(object).map(k => { return object[k]; }).join(', ');
}
export function getScrollBarWidth() {
  const inner = document.createElement('p');
  inner.style.width = '100%';
  inner.style.height = '200px';
  const outer = document.createElement('div');
  outer.style.position = 'absolute';
  outer.style.top = '0px';
  outer.style.left = '0px';
  outer.style.visibility = 'hidden';
  outer.style.width = '200px';
  outer.style.height = '150px';
  outer.style.overflow = 'hidden';
  outer.appendChild(inner);
  document.body.appendChild(outer);
  const w1 = inner.offsetWidth;
  outer.style.overflow = 'scroll';
  let w2 = inner.offsetWidth;
  if (w1 === w2) {
    w2 = outer.clientWidth;
  }
  document.body.removeChild(outer);
  return (w1 - w2);
}
export function getPageSize() {
  const bodyWidth = document.body.offsetWidth;
  if (bodyWidth < 768) {
    return 'xs';
  } else if (bodyWidth < 992) {
    return 'sm';
  } else if (bodyWidth < 1200) {
    return 'md';
  }
  return 'lg';
}


window.windowBlinking = [];


export function blinkTitle(msg1, msg2, delay, isFocus, timeout) {
  if (isFocus == null) {
    isFocus = false;
  }

  if (timeout == null) {
    timeout = false;
  }

  if (timeout) {
    setTimeout(blinkTitleStop, timeout);
  }

  document.title = msg1;

  if (isFocus == false) {
    window.windowBlinking.push(window.setInterval(() => {
      if (document.title == msg1) {
        document.title = msg2;
      } else {
        document.title = msg1;
      }
    }, delay));
  }

  if (isFocus == true) {
    let onPage = false;
    let testflag = true;

    const initialTitle = document.title;

    window.onfocus = function () {
      onPage = true;
    };

    window.onblur = function () {
      onPage = false;
      testflag = false;
    };


    window.windowBlinking.push(window.setInterval(() => {
      if (onPage == false) {
        if (document.title == msg1) {
          document.title = msg2;
        } else {
          document.title = msg1;
        }
      }
    }, delay));
  }
}

export function blinkTitleStop(title) {
  window.windowBlinking.forEach(it => {
    clearInterval(it);
  });
  if (!title) { title = window.documentTitle; }
  document.title = title || document.title;
}

export function playSoundNotify() {
  if (!window.playSoundNotify) {
    window.playSoundNotify = true;
    let filename = 'sound_a.mp3',
      audio = new Audio(filename);
    audio.play();
  }
}

export function createBrowserTabId() {
  if (!window.BrowserTabId) {
    window.BrowserTabId = uuid.v4();
  }
}
